const express = require("express"); //para magamit yung express features

const app = express(); // new express application

const port = 3000;

// Middleware: para maprevent yung problems sa ibang OS
app.use(express.json())//first middleware
app.use(express.urlencoded({extended : true}));//para maintindihan ni express yung url 


// ROUTES -------------------------------------------

// GET Method
app.get("/", (req, res) => {
	res.send("Hello World!");
});

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint.");
});

//create a new endpoint with your firstname
app.get("/donny", (req, res) => {
	res.send("Hello I am Donny Almero!");
});


// POST METHOD
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// another example with conditional
let  users = [];
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	} else {
		res.send(`Di pwede ang blank`);
	}
});


// PUT METHOD

app.put("/change-password", (req, res) => {
	let message;

	for(let i=0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username} password has been updated!`;
			break;
		} else {
			message = "User does not exist.";
		}
	}

	res.send(message);
	console.log(req.body);
});


// Discussion - Activity
app.get("/home", (req, res) => {
	res.send("Welcome to homepage")
});

app.get("/users", (req, res) => {
	res.send(users);
});

app.delete("/delete-user", (req, res) => {
	let message;

	if(users.length != 0){
		for(let i = 0; i < users.length; i++){
			if(req.body.username == users[i].username){
				users.splice(users[i], 1);
				message = `User ${req.body.username} has been deleted`;
				break
			} else {
				message = "User does not exist.";
			}
		}
	} else {
		message = "User has no data";
	}

	res.send(message);
});



app.listen(port, () => console.log(`Server is running at port ${port}`));